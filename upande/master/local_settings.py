# -*- coding: utf-8 -*-

###############################################
# Geosite local settings
###############################################
import os

# Outside URL
SITEURL = 'http://master.ngenokevin.com/'
# SITEURL = 'http://geonode.ngenokevin.com'
SERVE_PATH = '/var/www/geonode/master/'

# databases unique to site if not defined in site settings
# database info
# DATABASES = {
#     'default': {
#          'ENGINE': 'django.db.backends.postgresql_psycopg2',
#          'NAME': 'spatialsite',
#          'USER': 'postgres',
#          'PASSWORD': 'postgres',
#      },
#     # vector datastore for uploads
#     'master' : {
#         'ENGINE': 'django.contrib.gis.db.backends.postgis',
#          #'ENGINE': '', # Empty ENGINE name disables 
#         'NAME': 'spatialsite_data',
#         'USER' : 'postgres',
#         'PASSWORD' : 'postgres',
#         'HOST' : 'localhost',
#         'PORT' : '5432',
#     }
# }

LOCAL_ROOT = os.path.abspath(os.path.dirname(__file__))

# GEOSERVER_URL = SITEURL + 'geoserver/'
GEOSERVER_URL = 'http://geonode.ngenokevin.com/geoserver/'


# OGC (WMS/WFS/WCS) Server Settings
# OGC_SERVER = {
#     'default' : {
#         'BACKEND' : 'geonode.geoserver',
#         'LOCATION' : 'http://localhost:8080/geoserver/',
#         'PUBLIC_LOCATION' : 'http://geonode.ngenokevin.com/geoserver/',
#         'USER' : 'admin',
#         'PASSWORD' : 'geoserver',
#         'MAPFISH_PRINT_ENABLED' : True,
#         'PRINT_NG_ENABLED' : True,
#         'GEONODE_SECURITY_ENABLED' : True,
#         'GEOGIG_ENABLED' : True,
#         'WMST_ENABLED' : True,
#         'BACKEND_WRITE_ENABLED': True,
#         'WPS_ENABLED' : True,
#         'LOG_FILE': '%s/geoserver/data/logs/geoserver.log' % os.path.abspath(os.path.join(LOCAL_ROOT, os.pardir)),
#         # Set to name of database in DATABASES dictionary to enable
#         'DATASTORE': 'master', #'datastore',
#     }
# }

