# -*- coding: utf-8 -*-

###############################################
# Geosite settings
###############################################

import os
from geonode.contrib import geosites

# Directory of master site - for GeoSites it's two up
GEOSITES_ROOT = os.path.dirname(geosites.__file__)
SITE_ROOT = os.path.dirname(__file__)

try:
    # read in project pre_settings
    execfile(os.path.join(SITE_ROOT, '../', 'pre_settings.py'))
except:
    # if not available, read in GeoSites pre_settings
    execfile(os.path.join(GEOSITES_ROOT, 'pre_settings.py'))


SITE_ID = 2
SITE_NAME = 'Master'
# Should be unique for each site
SECRET_KEY = "2xpjOgdlPToJVshjWJ"

# site installed apps
SITE_APPS = ()

# Site specific databases
SITE_DATABASES = {}

AUTH_USER_MODEL = os.getenv('AUTH_USER_MODEL','people.Profile')

##### Overrides
# Below are some common GeoNode settings that might be overridden for site

# admin email
THEME_ACCOUNT_CONTACT_EMAIL = ''

# Have GeoServer use this database for this site
DATASTORE = 'master'

# Allow users to register
REGISTRATION_OPEN = True

#ACCOUNT_EMAIL_CONFIRMATION_EMAIL=True
#ACCOUNT_EMAIL_CONFIRMATION_REQUIRED=True

ALLOWED_HOSTS= ['localhost', '45.55.208.126','master.ngenokevin.com']
# uncomment for production
PROXY_ALLOWED_HOSTS = ('45.55.208.126','107.170.107.225','gis.washmis.com','geonode.ngenokevin.com',)

# # localhost by default
PROXY_BASE_URL = "http://geonode.ngenokevin.com/"

# PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

# MEDIA_ROOT = os.path.join(PROJECT_ROOT, "uploaded")

# STATIC_ROOT = os.path.join(PROJECT_ROOT, "static_root")

# MEDIA_URL = "/uploaded/"

# STATIC_URL = "/static/"

# MEDIA_ROOT = '/var/www/geonode/uploaded/master'
# STATIC_ROOT = '/var/www/geonode/static/master'

# Read in GeoSites post_settings
try:
    # read in project pre_settings
    execfile(os.path.join(SITE_ROOT, '../', 'post_settings.py'))
except:
    # if not available, read in GeoSites pre_settings
    execfile(os.path.join(GEOSITES_ROOT, 'post_settings.py'))
