import os
import sys
import site


envpath = '/home/venv/geosites/lib/python2.7/site-packages'
#envpath2 = '/home/mamase/venv/geonodems/lib/python2.7/site-packages'
# we add currently directory to path and change to it
pwd = os.path.dirname(os.path.abspath(__file__))
os.chdir(pwd)
sys.path = [pwd] + sys.path

# Append paths
site.addsitedir(envpath)
#site.addsitedir(envpath2)
sys.path.append('/home/upande/upande/')
sys.path.append('/home/upande/upande/virtualkenya/')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "virtualkenya.settings")
activate_env=os.path.expanduser("/home/venv/geosites/bin/activate_this.py")
execfile(activate_env, dict(__file__=activate_env))
# This application object is used by any WSGI server configured to use this
# file. This includes Django's development server, if the WSGI_APPLICATION
# setting points here.
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()