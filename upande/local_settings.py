# -*- coding: utf-8 -*-

###############################################
# Geosite local settings
###############################################

# path for static and uploaded files
SERVE_PATH = '/home/geosites/'

# database info
DATABASES = {
    'default': {
         'ENGINE': 'django.db.backends.postgresql_psycopg2',
         'NAME': 'geonode',
         'USER': 'geonode',
         'PASSWORD': 'DFRsdfRW',
     },
    # vector datastore for uploads
    'master' : {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
         #'ENGINE': '', # Empty ENGINE name disables 
        'NAME': 'geonode_data',
        'USER' : 'geonode',
        'PASSWORD' : 'DFRsdfRW',
        'HOST' : 'localhost',
        'PORT' : '5432',
    }
}

# GEOSERVER_URL = SITEURL + 'geoserver/'

# # OGC (WMS/WFS/WCS) Server Settings
# OGC_SERVER = {
#     'default' : {
#         'BACKEND' : 'geonode.geoserver',
#         'LOCATION' : 'http://localhost:8080/geoserver/',
#         'PUBLIC_LOCATION' : GEOSERVER_URL,
#         'USER' : 'admin',
#         'PASSWORD' : 'geoserver',
#         'MAPFISH_PRINT_ENABLED' : True,
#         'PRINT_NG_ENABLED' : True,
#         'GEONODE_SECURITY_ENABLED' : True,
#         'GEOGIG_ENABLED' : True,
#         'WMST_ENABLED' : True,
#         'BACKEND_WRITE_ENABLED': True,
#         'WPS_ENABLED' : True,
#         'LOG_FILE': '%s/geoserver/data/logs/geoserver.log' % os.path.abspath(os.path.join(LOCAL_ROOT, os.pardir)),
#         # Set to name of database in DATABASES dictionary to enable
#         'DATASTORE': 'datastore', #'datastore',
#     }
# }

# email account for sending email
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_PORT = 587
EMAIL_USE_TLS = True

GEOSERVER_URL = 'http://geonode.ngenokevin.com/geoserver/'

#REGISTRATION_OPEN=True
#ACCOUNT_EMAIL_CONFIRMATION_EMAIL=True
#ACCOUNT_EMAIL_CONFIRMATION_REQUIRED=True

ALLOWED_HOSTS= ['localhost', '45.55.208.126','geonode.ngenokevin.com']
# uncomment for production
PROXY_ALLOWED_HOSTS = ('45.55.208.126','107.170.107.225','gis.washmis.com','geonode.ngenokevin.com',)

# # localhost by default
PROXY_BASE_URL = "http://geonode.ngenokevin.com/"

# PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

# MEDIA_ROOT = os.path.join(PROJECT_ROOT, "uploaded")

# MEDIA_URL = "/uploaded/"

# STATIC_ROOT = os.path.join(PROJECT_ROOT, "static_root")

# STATIC_URL = "/static/"
# MEDIA_ROOT = '/var/www/geonode/uploaded'
# STATIC_ROOT = '/var/www/geonode/static'


